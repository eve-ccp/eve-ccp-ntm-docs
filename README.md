# eve-ccp-ntm-docs

## Getting started

```mermaid
graph TD;
    A[加入南天门公开招新群]-->B[根据等级申请入团];
    B-->|T9以上|C[申请A团ccpa];
    B-->|T9以上|E[申请B团ccpb];

    C-->D[游戏中接收入团邮件, 加入CCP集团大群];
    E-->D;
    D-->F[查看大群公告]
    F-->G[查看军团制度]
    G-->H[查看协作表格<br>加入YY及集结通知群]
    H-->I[填写登记表]
    I-->J[资料审核成功被管理拉入在编人员群<br>军团职务成为注册成员]
```